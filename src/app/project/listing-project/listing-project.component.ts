import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-listing-project',
  templateUrl: './listing-project.component.html',
  styleUrls: ['./listing-project.component.scss']
})
export class ListingProjectComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  
  editField: string;
    personList: Array<any> = [
      { id: 1, Project_Name: 'Pangu', Project_Manager: 'Aurelia Vega', Project_Client: 'Unilever', Start_Date: '16 June 2020', End_Date: '25 Sept 2020'},
      { id: 2, Project_Name: 'Nuwa', Project_Manager: 'Aurelia Vega', Project_Client: 'Coca-Cola', Start_Date: '15 Jan 2019', End_Date: '30 Mar 2019'},
      { id: 3, Project_Name: 'Zeus', Project_Manager: 'Aurelia Vega', Project_Client: 'SAB', Start_Date: '02 Feb 2021', End_Date: '05 May 2021'},
      { id: 4, Project_Name: 'Osiris', Project_Manager: 'Aurelia Vega', Project_Client: 'Unilever', Start_Date: '01 Apr 2020', End_Date: '06 Jul 2020'},
      { id: 5, Project_Name: 'MoonShot', Project_Manager: 'Aurelia Vega', Project_Client: 'RedBull', Start_Date: '26 October 2018', End_Date: '1 Dec 2018' },
    ];

    updateList(id: number, property: string, event: any) {
      const editField = event.target.textContent;
      this.personList[id][property] = editField;
    }

    add() {
      
    }

    changeValue(id: number, property: string, event: any) {
      this.editField = event.target.textContent;
    }
}
