import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-add-edit-project',
  templateUrl: './add-edit-project.component.html',
  styleUrls: ['./add-edit-project.component.scss']
})
export class AddEditProjectComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  addProject(){
    console.log("new project added ");
  }
  projectForm: FormGroup;
}
