import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-listing-client',
  templateUrl: './listing-client.component.html',
  styleUrls: ['./listing-client.component.scss']
})
export class ListingClientComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  
  editField: string;
    personList: Array<any> = [
      { id: 1, Client_Name: 'Aurelia Vega', Client_Address: '2059 Glyn St', Client_Phone_Number: '083 557 1646', Client_Email: 'Vega@gmail.com', Client_Status: 'Active' },
      { id: 2, Client_Name: 'Guerra Cortez', Client_Address: '1877 Bad St', Client_Phone_Number: '083 844 7482', Client_Email: 'Cortez@hotmail.com', Client_Status: 'active' },
      { id: 3, Client_Name: 'Guadalupe House', Client_Address: '478 Doreen St', Client_Phone_Number: '085 243 6461', Client_Email: 'House@yahoo.com', Client_Status: 'unactive' },
      { id: 4, Client_Name: 'Ichi Nobu', Client_Address: '2263 Nelson Mandela Drive', Client_Phone_Number: '083 676 6632', Client_Email: 'Nobu@outlook.com', Client_Status: 'active' },
      { id: 5, Client_Name: 'Elisa Gallagher', Client_Address: '110 Glyn St', Client_Phone_Number: '085 594 1996', Client_Email: 'Gallagher@gmail.com', Client_Status: 'unactive' },
    ];


    updateList(id: number, property: string, event: any) {
      const editField = event.target.textContent;
      this.personList[id][property] = editField;
    }

    add() {
      
    }

    changeValue(id: number, property: string, event: any) {
      this.editField = event.target.textContent;
    }
}
