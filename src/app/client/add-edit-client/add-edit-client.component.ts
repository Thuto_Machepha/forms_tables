import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-edit-client',
  templateUrl: './add-edit-client.component.html',
  styleUrls: ['./add-edit-client.component.scss']
})
export class AddEditClientComponent implements OnInit {
  clientForm:FormGroup

  constructor(formBuilder:FormBuilder) { 
    this.setValidation(formBuilder);
  }

  ngOnInit(): void {
  }

  saveClient(){
    console.warn(this.clientForm.value);
  }

  setValidation(formBuilder){
    this.clientForm = formBuilder.group({
      clientName: ['', Validators.required],
      clientAddress: [''],
    });
  }

  get clientName() { return this.clientForm.get('clientName'); }

}
