import { NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { ClientComponent} from './client/client.component';
import { AddEditClientComponent } from './client/add-edit-client/add-edit-client.component';
import { ListingClientComponent } from './client/listing-client/listing-client.component';
import { ProjectComponent } from './project/project.component';
import { AddEditProjectComponent } from './project/add-edit-project/add-edit-project.component';
import { ListingProjectComponent } from './project/listing-project/listing-project.component';

const routes: Routes = [{path:'',component:HeaderComponent},
{path:'client', component:ClientComponent},
{path:'client/add-edit-client',component:AddEditClientComponent},
{path:'client/listing-client',component:ListingClientComponent},
{path:'project', component:ProjectComponent},
{path:'project/add-edit-project' ,component:AddEditProjectComponent},
{path:'project/listing-project' ,component:ListingProjectComponent}
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports:[RouterModule]
})
export class AppRoutingModule { }
