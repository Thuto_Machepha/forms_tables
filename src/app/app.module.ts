import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ClientComponent } from './client/client.component';
import { ProjectComponent } from './project/project.component';
import { AddEditClientComponent } from './client/add-edit-client/add-edit-client.component';
import { ListingClientComponent } from './client/listing-client/listing-client.component';
import { AddEditProjectComponent } from './project/add-edit-project/add-edit-project.component';
import { ListingProjectComponent } from './project/listing-project/listing-project.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ClientComponent,
    ProjectComponent,
    AddEditClientComponent,
    ListingClientComponent,
    AddEditProjectComponent,
    ListingProjectComponent
  ],
  imports: [
    BrowserModule,
    MDBBootstrapModule.forRoot(),
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
